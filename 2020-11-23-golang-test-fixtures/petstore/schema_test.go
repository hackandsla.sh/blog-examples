package petstore_test

import (
	"context"
	"testing"

	"testfixtures/petstore"
	"testfixtures/test"
)

func TestSchemaValidation(t *testing.T) {
	schema, err := test.GetPetSchema()
	if err != nil {
		t.Fatal(err)
	}

	pet := petstore.Pet{
		ID:   42,
		Name: "foo",
	}

	errs := schema.Validate(context.TODO(), pet).Errs
	if len(*errs) > 0 {
		t.Errorf("got the following errors while validating data: %v", errs)
	}
}
