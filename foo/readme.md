<!-- Code generated by gomarkdoc. DO NOT EDIT -->

# foo

```go
import "foo"
```

## Index

- [func Hello(name string) string](<#func-hello>)


## func Hello

```go
func Hello(name string) string
```

<details><summary>Example</summary>
<p>

```go
{
	fmt.Println(Hello("foo"))

}
```



Outputs:

```output
Hello foo!
```

</p>
</details>



Generated by [gomarkdoc](<https://github.com/princjef/gomarkdoc>)
