module foo

go 1.14

require (
	github.com/princjef/gomarkdoc v0.1.1 // indirect
	github.com/spf13/cobra v1.1.1 // indirect
	github.com/spf13/viper v1.7.1 // indirect
)
