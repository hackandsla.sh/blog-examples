package main

import (
	"context"
	"log"
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/hackandsla.sh/blog-examples/startup/httpserver"
	"gitlab.com/hackandsla.sh/blog-examples/startup/jsondb"
	"gitlab.com/hackandsla.sh/blog-examples/startup/reddit"
)

func main() {
	db := jsondb.New("reddit.json")
	httpSvc := httpserver.New(db)
	redditPoller := reddit.New(db)

	if err := db.Start(); err != nil {
		log.Println(err)
		return
	}
	defer db.Stop()

	errChan := make(chan error)

	httpSvc.Start(errChan)
	defer httpSvc.Stop()

	ctx, cancel := context.WithCancel(context.Background())
	redditPoller.Start(ctx)
	defer cancel()

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt, syscall.SIGTERM)

	for {
		select {
		case <-quit:
			return
		case err := <-errChan:
			log.Printf("got gitlab.com/hackandsla.sh/blog-examples/startup error: %v\n", err)
			return
		}
	}
}
